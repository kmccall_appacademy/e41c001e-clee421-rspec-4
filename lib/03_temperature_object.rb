class Temperature
  def initialize(opts={})
    @options = opts
    #Considered writing the conversion here but this seems like storage
    #doubling if the other degrees is never called
  end

  def in_fahrenheit
    if @options.has_key?(:f)
      return @options[:f]
    elsif @options.has_key?(:c)
      @options[:f] = ((@options[:c] * 1.8) + 32).round(1)
      return @options[:f]
    end
  end

  def in_celsius
    if @options.has_key?(:c)
      return @options[:c]
    elsif @options.has_key?(:f)
      @options[:c] = ((@options[:f] - 32) / 1.8).round(1)
      return @options[:c]
    end
  end

  def self.from_fahrenheit(f)
    self.new({:f => f})
  end

  def self.from_celsius(c)
    self.new({:c => c})
  end
end

class Celsius < Temperature
  def initialize(c=nil)
    @celsius = c
  end

  def in_fahrenheit
    return ((@celsius * 1.8) + 32).round(1) if @celsius != nil
  end

  def in_celsius
    return @celsius if @celsius != nil
  end
end

class Fahrenheit < Temperature
  def initialize(f=nil)
    @fahrenheit = f
  end

  def in_celsius
    return ((@fahrenheit - 32) / 1.8) if @fahrenheit != nil
  end

  def in_fahrenheit
    return @fahrenheit if @fahrenheit != nil
  end
end
