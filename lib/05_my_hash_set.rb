# MyHashSet
#
# Ruby provides a class named `Set`. A set is an unordered collection of
# values with no duplicates.  You can read all about it in the documentation:
#
# http://www.ruby-doc.org/stdlib-2.1.2/libdoc/set/rdoc/Set.html
#
# Let's write a class named `MyHashSet` that will implement some of the
# functionality of a set. Our `MyHashSet` class will utilize a Ruby hash to keep
# track of which elements are in the set.  Feel free to use any of the Ruby
# `Hash` methods within your `MyHashSet` methods.
#
# Write a `MyHashSet#initialize` method which sets an empty hash object to
# `@store`. Next, write an `#insert(el)` method that stores `el` as a key
# in `@store`, storing `true` as the value. Write an `#include?(el)`
# method that sees if `el` has previously been `insert`ed by checking the
# `@store`; return `true` or `false`.
#
# Next, write a `#delete(el)` method to remove an item from the set.
# Return `true` if the item had been in the set, else return `false`.  Add
# a method `#to_a` which returns an array of the items in the set.
#
# Next, write a method `set1#union(set2)` which returns a new set which
# includes all the elements in `set1` or `set2` (or both). Write a
# `set1#intersect(set2)` method that returns a new set which includes only
# those elements that are in both `set1` and `set2`.
#
# Write a `set1#minus(set2)` method which returns a new set which includes
# all the items of `set1` that aren't in `set2`.

class MyHashSet
  def initialize(init={})
    @store = init
  end #initialize

  def insert(el)
    @store[el] = true
  end #insert

  def include?(el)
    @store.key?(el)
  end #include?

  def delete(el)
    if include?(el)
      @store.delete(el)
      return true
    else
      return false
    end
  end #delete

  def to_a
    @store.keys
  end #to_a

  def union(set2)
    new_myset = MyHashSet.new
    set2.to_a.each {|k| new_myset.insert(k) unless new_myset.include?(k)}
    self.to_a.each {|k| new_myset.insert(k) unless new_myset.include?(k)}
    new_myset
  end #union

  def intersect(set2)
    new_myset = MyHashSet.new
    set2.to_a.each {|k| new_myset.insert(k) if self.include?(k)}
    self.to_a.each {|k| new_myset.insert(k) if set2.include?(k)}
    new_myset
  end #intersect

  def minus(set2)
    new_myset = MyHashSet.new
    self.to_a.each {|k| new_myset.insert(k) unless set2.include?(k)}
    new_myset
  end #minus

  def symmetric_difference(set2)
    new_myset = MyHashSet.new
    set2.to_a.each {|k| new_myset.insert(k) unless self.include?(k)}
    self.to_a.each {|k| new_myset.insert(k) unless set2.include?(k)}
    new_myset
  end #symmetric_difference

  def size
    @store.length
  end #size

  def ==(set2)
    if self.size != set2.size
      return false
    else
      set2.to_a.each {|k| return false unless self.include?(k)}
    end
    true
  end #==
end #MyHashSet

# Bonus
#
# - Write a `set1#symmetric_difference(set2)` method; it should return the
#   elements contained in either `set1` or `set2`, but not both!
# - Write a `set1#==(object)` method. It should return true if `object` is
#   a `MyHashSet`, has the same size as `set1`, and every member of
#   `object` is a member of `set1`.
