class Dictionary
  def initialize(opts={})
    @entries = opts
  end

  def entries
    @entries
  end

  def add(hash_or_key)
    if hash_or_key.is_a?(Hash)
      @entries = @entries.merge(hash_or_key)
    else
      @entries[hash_or_key] = nil
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(key)
    @entries.key?(key)
  end

  def find(word)
    res = {}
    @entries.each_key do |k|
      res[k] = @entries[k] if (k =~ /#{word}/) == 0
    end
    res
  end

  def printable
    print = []
    sorted = @entries.sort_by {|k,v| k}
    sorted.each do |d|
      print << "[#{d.first}] \"#{d.last}\""
    end
    print.join("\n")
  end
end
