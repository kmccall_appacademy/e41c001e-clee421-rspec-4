class Book
  def initialize
    @title
    @no_cap_list = ["and", "in", "the", "of", "a", "an"]
  end

  def title=(s)
    t = s.split.map {|w| @no_cap_list.include?(w) ? w.downcase : w.capitalize}.join(' ')
    t[0] = t[0].upcase
    @title = t
  end

  def title
    @title
  end
end
