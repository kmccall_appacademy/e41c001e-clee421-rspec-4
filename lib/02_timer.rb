class Timer

def initialize(seconds=0)
  @seconds = seconds
end

def seconds
  @seconds
end

def seconds=(s)
  @seconds = s
end

def time_string
  #It looks better with the variables
  hour = (@seconds / 3600).to_s.rjust(2, '0')
  min = ((@seconds % 3600) / 60).to_s.rjust(2, '0')
  sec = (@seconds % 60).to_s.rjust(2, '0')
  "#{hour}:#{min}:#{sec}"
end

end
